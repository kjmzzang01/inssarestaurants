import urllib.request
from urllib.parse import quote
import random
from bs4 import BeautifulSoup
from flask import Flask, request
from slack import WebClient
from slackeventsapi import SlackEventAdapter
import requests
import json
from config import *
import string

# main
app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)
url = "https://search.naver.com/search.naver?sm=top_hty&fbm=1&ie=utf8&query="


def find_pop_rest(text):
    bot_input = text.split()
    totallist = []

    # 상단 헤드 텍스트 부분
    intro = {
        "pretext": "*" + bot_input[2] + "에 나온 " + bot_input[1] + " 맛집 입니다*\n 가게명을 클릭하시면 네이버 상세페이지로 이동 됩니다."
    }
    totallist.append(intro)

    # 크롤링 URL
    u = url+quote(bot_input[1]+" "+bot_input[2])
    source_code = urllib.request.urlopen(u).read()
    soup = BeautifulSoup(source_code, "html.parser")
    # 바디 내용
    for i in soup.find_all("div", class_="list_area"):
        for j in i.find_all("ul", class_="list_place_col1"):  # 리스트
            for k in j.find_all("li"):  # 하나의 식당
                subtext = []
                subdic = {}
                r = lambda: random.randint(0, 255)
                subdic["color"] = "#%02X%02X%02X" % (r(), r(), r())  # 섹션 왼쪽 색상바
                for l in k.find_all("div", class_="info_area"):
                    for q in l.find_all("div", class_="tit"):
                        for w in q.find_all("a"):
                            for e in w.find_all("span"):
                                subdic["author_name"] = e.get_text()  # 식당 이름
                                subdic["author_link"] = q.find('a')['href']  # 식당 네이버 플레이트 url
                        for w in q.find_all("span", class_="category"):
                            subtext.append(w.get_text())  # 식당 분류
                    for q in l.find_all("div", class_="txt"):
                        for w in q.find_all("span", class_="menu"):
                            subtext.append(w.get_text()+"\n")  # 식당 대표 메뉴
                        for w in q.find_all("span", class_="date"):
                            subtext.append(w.get_text())  # 프로그램 방영 일자
                subdic["text"] = ('\t'.join(subtext))   # 식당 분류+ 대표메뉴+ 방영일자를 종합
                subdic["image_url"] = k.find('img')['src']  # 식당 메인 사진
                totallist.append(subdic)  # 하나의 식당을 섹션으로 저장

    # 하단 푸터 더보기 버튼
    footer = {"text": bot_input[1]+"의 더많은 맛집을 보시려면",
              "actions": [
                   {
                       "type": "button",
                       "text": "여기를 눌러주세요!",
                       "url": "https://store.naver.com/restaurants/list?entry=pll&program=" + quote(bot_input[2]) + "&query=" + quote(
                           bot_input[1]) + "%20" + quote(bot_input[2]) + quote("맛집") + "&region=" + quote(bot_input[1])
                   }
               ]
              }
    totallist.append(footer)
    return totallist


# tv 리스트를 두줄로 보여주는 함수
def tv_lst(lst):
    totallist = []
    fields = []
    title = {
        "title": "TV 프로그램 리스트 입니다."
    }
    for i in lst:
        fields.append(
            {
                "title": i,
                "short": "true"
            }
        )
    totallist.append(title)
    totallist.append({"fields": fields})
    return totallist


# 머신러닝으로 텍스트 뽑는 함수
def get_answer(text, user_key):
    text = text.replace("<@ULCVDF65D>", "")
    for symbol in string.punctuation+' ':
        text = text.replace(symbol, "")
    data_send = {
        'query': text,
        'sessionId': 'a',
        'lang': 'ko',
    }

    data_header = {
        'Authorization': BEARER,
        'Content-Type': 'application/json; charset=utf-8'
    }

    dialogflow_url = 'https://api.dialogflow.com/v1/query?v=20150910'

    res = requests.post(dialogflow_url, data=json.dumps(data_send), headers=data_header)
    if res.status_code != requests.codes.ok:
        return '오류가 발생했습니다.'

    data_receive = res.json()

    answer = data_receive['result']['fulfillment']['speech']

    return answer


# 챗봇이 멘션을 받았을 경우
multiCheck = "asdasd"
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]
    slackid = event_data["event"]["client_msg_id"]
    global multiCheck

    if text == multiCheck:
        return
    multiCheck = slackid

    tv_list_view = tv_lst(tv_list)

    # 봇만 호출하였을때
    if len(text.split()) == 1:
        slack_web_client.chat_postMessage(
            channel=channel,
            text=HELP_MENT,
            attachments=tv_list_view
        )
        return

    # 머신러닝으로 텍스트 뽑아오기
    userid = request.args.get('userid')
    ex_text = get_answer(text, userid)

    bot_input = ex_text.split()

    # 봇과 이상한것만 호출하였을 때
    # 봇과 명령어를 1개만 호출하였을 때
    if len(bot_input) == 1 or len(bot_input) == 2:
        slack_web_client.chat_postMessage(
            channel=channel,
            text=HELP_MENT,
            attachments=tv_list_view
        )
        return

    # tv프로그램이 유효한지 체크
    remention = True
    if bot_input[2] in tv_list:
        remention = False

    if (remention):
        slack_web_client.chat_postMessage(
            channel=channel,
            text=" *tv프로그램* 을 잘못입력하셨습니다 \n "+HELP_MENT,
            attachments=tv_list_view
        )
        return
    # 봇 지역 TV프로그램 을 입력하였을때 (검색 정보가 존재 하지 않을때)

    message = find_pop_rest(ex_text)
    if len(message) == 2:
        slack_web_client.chat_postMessage(
            channel=channel,
            text="*" + bot_input[2] + "* 의 *" + bot_input[1] + "* 맛집이 존재하지 않습니다. \n"+HELP_MENT,
            attachments=tv_list_view
        )
        return
    # 봇 지역 TV프로그램 을 입력하였을때 (검색 성공)
    slack_web_client.chat_postMessage(
        channel=channel,
        attachments=message
    )


@app.route("/", methods=["POST","GET"])
def index():
    return "<h1>Server is ready.</h1>"


if __name__ == '__main__':
    app.run('0.0.0.0', port=5000)
